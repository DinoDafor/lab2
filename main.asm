extern read_word_with_space
extern read_word
extern string_copy
extern string_length
extern find_word
extern print_string
extern print_char
extern print_newline
global _start

section .data
key_error_message: db "There is no such key in the dictionary.",0
key_too_long_error_message: db "Key is too long, try shorter key.",0
buffer: times 256 db 0

section .text
%include "colon.inc"
%include "words.inc"

_start:
	mov rdi,buffer	; Адрес, куда будет считывать слово.
	mov rsi,256	; Размер буфера.
	call read_word_with_space ; Читаем ключ.
	test rax, rax	; Проверяем на то, что строка из ввода поместилась в буфер.
	jz .long_error	; Если нет, то выводим ошибку.

	push rdx 	; Сохраняем длину строки, которая лежит в буфере.
	mov rdi,rax	; В А лежит указатель на буфер. Кладем в rdi
	mov rsi,pointer ; poiner это указатель на последнее добавленное слово в словарь. Определяется в colon.inc.
	call find_word  ; Ищем совпадение ключей.
	test rax,rax 	; Если 0, то ключ не найден.
	jz .error	; Переход

	add rax,8 	; Переходим к адресу ключа.
	push rax 	; Сохраняем адрес ключа.
	mov rdi,rax 	; Кладём указатель на ключ.
	call string_length ; Определяем длину искомого ключа.
	push rax 		; Сохраняем длину искомого ключа из А.
	call print_newline	;
	pop rax 	; Достаём длину искомого ключа в А.
	pop rdi		; Достём адрес ключа и кладём в rdi.
	add rdi, rax	; Прибавляем длину строки-ключа.
	inc rdi		; Увеличиваем её на 1, потому что у нас есть нуль-терм в конце. Теперь rdi указывает на описание слова.
	call print_string ; Выводим описание слова.
	call print_newline
.end:
	mov rax,60	; exit syscall
	xor rdi,rdi
	syscall
.error:
	call print_newline
	mov rdi,key_error_message	; Выводим сообщение, что такого ключа нет в словаре.
	call string_length		;
	mov rdx,rax			; Длина строки.
	mov rax,1 			; Номер системного вызова write.
	mov rdi,2 			; Номер дескриптора stderror.
	mov rsi,key_error_message	; Адрес.
	syscall
	call print_newline
	jmp .end

.long_error:
	call print_newline
        mov rdi, key_too_long_error_message      ; Выводим сообщение, что такого ключа нет в словаре.
        call string_length              ;
        mov rdx,rax                     ; Длина
        mov rax,1                       ; Номер системного вызова write.
        mov rdi,2                       ; Номер дескриптора stderror.
        mov rsi, key_too_long_error_message       ; Адрес.
        syscall
	call print_newline
        jmp .end

